# NOTEBOOKS FOR R&D

#### Docker commands
To build the docker image you're interested in:
```bash
docker-compose build
```

To launch the jupyter notebook you're interested in:
```bash
docker-compose up
```